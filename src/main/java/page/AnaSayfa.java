package page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.Random;

public class AnaSayfa extends mainClass {

    public AnaSayfa urlGit(String url) {
        driver.get(url);
        return this;
    }

    @Step("Rastgele Metin Oluşturma")
    public String createRandomText(int textSize) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < textSize; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();

        return output;
    }
    String email=createRandomText(7);
    @Step("Rastgele Email Doldurur")
    public AnaSayfa emailDoldur() {
        driver.findElement(By.id("signup:email")).sendKeys(email + "@gmail.com");
        return this;
    }

    @Step("Email Doldurur")
    public AnaSayfa eMailDoldur() {
        driver.findElement(By.id("login-form:email")).sendKeys(email+"@gmail.com");
        return this;
    }



    @Step("Giriş Yap")
    public AnaSayfa girisYap() {
        driver.findElement(By.id("login-form:login")).click();
        return this;
    }

    @Step("Kayıt 0l ekranına gider")
    public AnaSayfa kayitOlEkraniniAc() {
        //driver.findElement(By.cssSelector("[id='login-form'],[id='login-form:signup']")).click();
        driver.findElement(By.id("login-form:signup")).click();
        return this;
    }

    @Step("First Name Doldurur")
    public AnaSayfa fnameDoldur(String firstname) {

        driver.findElement(By.id("signup:fname")).sendKeys(firstname);
        return this;
    }

    @Step("Last Name Doldurur")
    public AnaSayfa lastNameDoldur(String lname) {
        driver.findElement(By.id("signup:lname")).sendKeys(lname);
        return this;
    }

    @Step("Rastgele Birth Date Doldurur")
    public AnaSayfa birthDoldur(int a, int b) {
        driver.findElement(By.xpath("//*[@id=\"BirthDate\"]")).sendKeys(a + "/" + b + "/" + "1992");
        return this;
    }



    @Step("Adres Bilgileri Doldurur")
    public AnaSayfa adresDoldur(String a, String b) {
        driver.findElement(By.id("signup:street")).sendKeys(a);
        driver.findElement(By.id("signup:city")).sendKeys(b);
        driver.findElement(By.id("signup:state")).sendKeys("Alaska");
        driver.findElement(By.id("signup:zip")).sendKeys("98545");
        return this;
    }

    @Step("Şifre doldur")
    public AnaSayfa sifreDoldur() {
        driver.findElement(By.id("signup:password")).sendKeys("123456789");
        return this;
    }

    @Step("Signup Butonuna Tıklar")
    public AnaSayfa butonaTikla() {
        driver.findElement(By.id("signup:signup")).click();
        return this;
    }

    @Step("Continue Butonuna Tıklar")
    public AnaSayfa conTıkla() {
        driver.findElement(By.id("signup:continue")).click();
        return this;
    }

    @Step("Logout Tıklar")
    public AnaSayfa logoutTikla() {
        driver.findElement(By.id("logout-form:logout")).click();
        return this;
    }
    @Step("Ana Sayfaya Döner")
    public AnaSayfa anasayfaTikla(){
        driver.findElement(By.xpath("//*[@id=\"home\"]/a")).click();
        return this;
    }
    @Step("Giriş yap ekranında şifre doldurur")
    public AnaSayfa passwordDoldur(){
        driver.findElement(By.id("login-form:password")).sendKeys("123456789");
        return this;
    }
}
