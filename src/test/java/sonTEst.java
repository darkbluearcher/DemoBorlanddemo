import io.qameta.allure.Step;
import org.testng.annotations.Test;
import page.AnaSayfa;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class sonTEst  {

    AnaSayfa anaSayfa= new AnaSayfa();


    @Step("Rastgele Metin Oluşturma")
    public String createRandomText(int textSize) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < textSize; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();

        return output;
    }
    @Step("Rastgele Sayı Oluşturma")
    public String createRandomNumber(int length) {
        Random r = new Random();
        List<Integer> digits = new ArrayList<Integer>();
        String number = "";

        for (int i = 0; i < length - 1; i++) {
            digits.add(i);
        }

        for (int i = length - 1; i > 0; i--) {
            int randomDigit = r.nextInt(i);
            number += digits.get(randomDigit);
            digits.remove(randomDigit);
        }

        number = "1" + number;

        return number;
    }
    @Test
    public void dene(){

        anaSayfa
                .urlGit("http://demo.borland.com/InsuranceWebExtJS/")
                .kayitOlEkraniniAc()
                .fnameDoldur(createRandomText(5))
                .lastNameDoldur(createRandomText(7))
                .birthDoldur(27,02)
                .emailDoldur()
                .adresDoldur(createRandomText(30),createRandomText(7))
                .sifreDoldur()
                .butonaTikla()
                .conTıkla()
                .anasayfaTikla()
                .logoutTikla()
                .eMailDoldur()
                .passwordDoldur()
                .girisYap();





    }
}
